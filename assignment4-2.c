//determine the given number is a prime number
#include<stdio.h>

int main()
{
    int n,i;
    int a=0,flag=0;

    printf("Enter the number: ");
    scanf("%d", &n);

    a=n/2;

    for(i=2 ; i<=a ; i++)
    {
        if(n%i==0)
        {
            printf("Not a prime number");
            flag=1;
            break;
        }
    }
    if(flag==0){
        printf("Prime number");
    }

    return 0;
}

