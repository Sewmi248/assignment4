//print multiplication tables from 1 to n
#include<stdio.h>

int main()
{
    int n, i, x, a=0;
    printf("Enter a number: ");
    scanf("%d", &n);

    for (x = 1; x <= n; ++x) {
        for (i = 1; i <= 12; ++i) {
            a+=1;
            printf("%d * %d = %d \n", x, a, x * a);
        }
        printf("\n");
        a = 0;
    }

    return 0;
}

